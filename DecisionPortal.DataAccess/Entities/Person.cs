﻿using System.ComponentModel.DataAnnotations;

namespace DecisionPortal.DataAccess.Entities
{
    public class Person : BaseEntity
    {
        [Required]
        public string Name { get; set; }
    }
}
