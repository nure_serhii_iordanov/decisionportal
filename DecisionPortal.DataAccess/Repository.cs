﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DecisionPortal.DataAccess.Entities;
using DecisionPortal.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DecisionPortal.DataAccess
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly DbContext _context;

        public Repository(DbContext context)
        {
            _context = context;
        }

        public IEnumerable<TEntity> Get()
        {
            return _context.Set<TEntity>().AsEnumerable();
        }

        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate)
        {
            return _context.Set<TEntity>().Where(predicate).AsEnumerable();
        }

        public TEntity Get(int id)
        {
            return Get(e => e.Id == id).FirstOrDefault();
        }

        public void Add(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
        }

        public void Delete(TEntity entity)
        {
            var existingEntity = _context.Set<TEntity>().Find(entity);
            if (existingEntity == null)
            {
                return;
            }

            _context.Set<TEntity>().Remove(existingEntity);
        }

        public void Update(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            _context.Set<TEntity>().Attach(entity);
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
