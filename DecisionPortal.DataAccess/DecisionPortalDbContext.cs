﻿using DecisionPortal.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace DecisionPortal.DataAccess
{
    public class DecisionPortalDbContext : DbContext
    {
        public DecisionPortalDbContext(DbContextOptions<DecisionPortalDbContext> options)
            : base(options)
        {
        }

        public DecisionPortalDbContext()
        {
        }

        public DbSet<Person> Persons { get; set; }
    }
}
