﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DecisionPortal.DataAccess.Entities;

namespace DecisionPortal.DataAccess.Interfaces
{
    public interface IRepository<TEntity> : IDisposable where TEntity : BaseEntity
    {
        IEnumerable<TEntity> Get();

        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate);

        TEntity Get(int id);

        void Add(TEntity entity);

        void Delete(TEntity entity);

        void Update(TEntity entity);
    }
}