﻿using System;
using System.Threading.Tasks;
using DecisionPortal.DataAccess.Entities;

namespace DecisionPortal.DataAccess.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity;

        Task CommitAsync();
    }
}