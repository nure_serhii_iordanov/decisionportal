﻿using System.Threading.Tasks;
using DecisionPortal.DataAccess.Entities;
using DecisionPortal.DataAccess.Interfaces;

namespace DecisionPortal.DataAccess
{
    /// <summary>
    /// Implements unit of work pattern.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DecisionPortalDbContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork"/> class.
        /// </summary>
        /// <param name="context">Current DbContext.</param>
        public UnitOfWork(DecisionPortalDbContext context)
        {
            _context = context;
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity
        {
            return new Repository<TEntity>(_context);
        }

        public Task CommitAsync()
        {
            return _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}