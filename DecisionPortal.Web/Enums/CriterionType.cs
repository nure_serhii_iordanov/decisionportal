﻿namespace DecisionPortal.Web.Enums
{
    public enum CriterionType
    {
        Qualitative = 1,
        Quantitative = 2
    }
}
