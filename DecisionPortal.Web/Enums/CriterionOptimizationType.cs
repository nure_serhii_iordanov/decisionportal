﻿namespace DecisionPortal.Web.Enums
{
    public enum CriterionOptimizationType
    {
        Max = 1,
        Min = 2
    }
}
