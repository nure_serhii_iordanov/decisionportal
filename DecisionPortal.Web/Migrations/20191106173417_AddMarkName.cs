﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DecisionPortal.Web.Migrations
{
    public partial class AddMarkName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Mark",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "Mark");
        }
    }
}
