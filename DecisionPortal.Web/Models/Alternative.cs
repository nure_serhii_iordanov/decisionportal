﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DecisionPortal.Web.Models
{
    [Table(nameof(Alternative))]
    public class Alternative : BaseEntity
    {
        [Required]
        public string Name { get; set; }
    }
}
