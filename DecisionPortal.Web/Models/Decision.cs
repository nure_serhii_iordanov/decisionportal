﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DecisionPortal.Web.Models
{
    [Table(nameof(Decision))]
    public class Decision : BaseEntity
    {
        public int Rank { get; set; }

        public float Weight { get; set; }

        public int PersonId { get; set; }

        [ForeignKey(nameof(PersonId))]
        public Person Person { get; set; }

        public int AlternativeId { get; set; }

        [ForeignKey(nameof(AlternativeId))]
        public Alternative Alternative { get; set; }
    }
}
