﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DecisionPortal.Web.Enums;

namespace DecisionPortal.Web.Models
{
    [Table(nameof(Criterion))]
    public class Criterion : BaseEntity
    {
        [Required]
        public string Name { get; set; }

        public int Rank { get; set; }

        public float Weight { get; set; }

        public CriterionType Type { get; set; }

        public CriterionOptimizationType OptimizationType { get; set; }

        [Required]
        public string MeasurementUnits { get; set; }

        public string ScaleType { get; set; }
    }
}
