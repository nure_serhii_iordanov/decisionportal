﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DecisionPortal.Web.Models
{
    [Table(nameof(AlternativeMark))]
    public class AlternativeMark : BaseEntity
    {
        public int AlternativeId { get; set; }

        [ForeignKey(nameof(AlternativeId))]
        public Alternative Alternative { get; set; }

        public int MarkId { get; set; }

        public Mark Mark { get; set; }
    }
}
