﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DecisionPortal.Web.Models
{
    [Table(nameof(Person))]
    public class Person : BaseEntity
    {
        [Required]
        public string Name { get; set; }

        public int CompetencyLevel { get; set; }
    }
}
