﻿using System.Collections.Generic;

namespace DecisionPortal.Web.Models.ViewModels
{
    public class AlternativeMarksViewModel
    {
        public Alternative Alternative { get; set; }

        public IDictionary<Criterion, IList<Mark>> Marks { get; set; }
    }
}
