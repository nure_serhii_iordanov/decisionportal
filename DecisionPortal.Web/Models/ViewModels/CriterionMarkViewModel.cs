﻿using System.Collections.Generic;

namespace DecisionPortal.Web.Models.ViewModels
{
    public class CriterionMarkViewModel
    {
        public Criterion Criterion { get; set; }

        public IEnumerable<Mark> AvailableMarks { get; set; }
    }
}
