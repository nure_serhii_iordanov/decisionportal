﻿using System.Collections.Generic;

namespace DecisionPortal.Web.Models.ViewModels
{
    public class AddAlternativeMarkViewModel
    {
        public Alternative Alternative { get; set; }

        public IEnumerable<int> SelectedMarkIds { get; set; }

        public IEnumerable<CriterionMarkViewModel> CriterionMarks { get; set; }
    }
}
