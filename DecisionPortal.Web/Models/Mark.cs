﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DecisionPortal.Web.Models
{
    [Table(nameof(Mark))]
    public class Mark : BaseEntity
    {
        public int Rank { get; set; }

        public string Name { get; set; }

        public float Value { get; set; }

        public float NormalizedValue { get; set; }

        public int CriterionId { get; set; }

        [ForeignKey(nameof(CriterionId))]
        public Criterion Criterion { get; set; }
    }
}
