﻿using System;
using System.Collections.Generic;

namespace DecisionPortal.Web.Utils
{
    public static class EnumHelper
    {
        public static IEnumerable<string> GetValues<TEnum>() where TEnum : Enum
        {
            return Enum.GetNames(typeof(TEnum));
        }
    }
}
