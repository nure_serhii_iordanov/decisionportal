﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DecisionPortal.Web;
using DecisionPortal.Web.Models;
using DecisionPortal.Web.Models.ViewModels;
using DecisionPortal.Web.Utils;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace DecisionPortal.Web.Controllers
{
    public class DecisionsController : Controller
    {
        private readonly DecisionPortalDbContext _context;

        public DecisionsController(DecisionPortalDbContext context)
        {
            _context = context;
        }

        // GET: Decisions
        public async Task<IActionResult> Index()
        {
            var decisions = await _context.Decisions.Include(d => d.Alternative).Include(d => d.Person).ToListAsync();
            ViewData["People"] = _context.Persons.ToList();
            ViewData["LeadingAlternativeBoard"] = GetLeadingAlternativeBoard(decisions);
            ViewData["LeadingAlternativeCopland"] = GetLeadingAlternativeCopland(decisions);

            return View(decisions);
        }

        private IEnumerable<(Alternative alternative, int score)> GetLeadingAlternativeBoard(IList<Decision> decisions)
        {
            var maxRank = decisions.Select(decision => decision.Rank).Max() + 1;

            var scores = new Dictionary<Alternative, IList<int>>();

            foreach (var decision in decisions)
            {
                if (scores.ContainsKey(decision.Alternative))
                {
                    scores[decision.Alternative].Add(maxRank - decision.Rank);
                }
                else
                {
                    scores.Add(decision.Alternative, new List<int> { maxRank - decision.Rank });
                }
            }

            var results = scores.Select(pair => (pair.Key, pair.Value.Sum())).OrderByDescending(tuple => tuple.Item2).ToList();

            return results;
        }

        private IEnumerable<(Alternative alternative, int score)> GetLeadingAlternativeCopland(IList<Decision> decisions)
        {
            var alternatives = _context.Alternatives.ToList();

            var personDecisions = new Dictionary<Person, IList<Decision>>();

            foreach (var decision in decisions)
            {
                if (personDecisions.ContainsKey(decision.Person))
                {
                    personDecisions[decision.Person].Add(decision);
                }
                else
                {
                    personDecisions.Add(decision.Person, new List<Decision> { decision });
                }
            }

            var scores = new Dictionary<Alternative, int>();

            for (int i = 0; i < alternatives.Count; i++)
            {
                var score = 0;

                for (int j = 0; j < alternatives.Count; j++)
                {
                    if (i != j)
                    {
                        foreach (var personDecision in personDecisions)
                        {
                            if (personDecision.Value.First(decision => decision.AlternativeId == alternatives[i].Id).Rank 
                                < personDecision.Value.First(decision => decision.AlternativeId == alternatives[j].Id).Rank)
                            {
                                score++;
                            }
                            else
                            {
                                score--;
                            }
                        }
                    }
                }

                scores.Add(alternatives[i], score);
            }

            return scores.Select(pair => (pair.Key, pair.Value)).OrderByDescending(tuple => tuple.Value);
        }

        // GET: Decisions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var decision = await _context.Decisions
                .Include(d => d.Alternative)
                .Include(d => d.Person)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (decision == null)
            {
                return NotFound();
            }

            return View(decision);
        }

        // GET: Decisions/Create
        public IActionResult Create(int id)
        {
            ViewData["PersonId"] = id;
            var currentPairs = HttpContext.Session.GetObject<IEnumerable<DecisionPair>>("todo" + id);

            if (currentPairs == null || !currentPairs.Any())
            {
                currentPairs = GetAllPossiblePairs();
                HttpContext.Session.SetObject("todo" + id, currentPairs);
            }

            var result = currentPairs.First(pair => !pair.Chosen.HasValue);

            var firstAlternative = new AlternativeMarksViewModel
            {
                Alternative = _context.Alternatives.Find(result.First),
                Marks = new Dictionary<Criterion, IList<Mark>>()
            };

            var secondAlternative = new AlternativeMarksViewModel
            {
                Alternative = _context.Alternatives.Find(result.Second),
                Marks = new Dictionary<Criterion, IList<Mark>>()
            };

            var firstCriteria = _context.Criteria.ToList();

            foreach (var criterion in firstCriteria)
            {
                var marks = _context.AlternativeMarks
                    .Where(altMark => altMark.AlternativeId == firstAlternative.Alternative.Id)
                    .Select(altMark => altMark.Mark)
                    .Where(mark => mark.CriterionId == criterion.Id)
                    .ToList();

                firstAlternative.Marks.Add(criterion, marks);
            }


            var secondCriteria = _context.Criteria.ToList();

            foreach (var criterion in secondCriteria)
            {
                var marks = _context.AlternativeMarks
                    .Where(altMark => altMark.AlternativeId == secondAlternative.Alternative.Id)
                    .Select(altMark => altMark.Mark)
                    .Where(mark => mark.CriterionId == criterion.Id)
                    .ToList();

                secondAlternative.Marks.Add(criterion, marks);
            }

            ViewData["Alternatives"] = new List<AlternativeMarksViewModel> {firstAlternative, secondAlternative};

            return View(new DecisionPair());
        }

        public class DecisionPair
        {
            public int First { get; set; }

            public int Second { get; set; }

            public int? Chosen { get; set; }

            public override bool Equals(object obj)
            {
                var toCompare = (DecisionPair) obj;

                return toCompare.First == First && toCompare.Second == Second ||
                       toCompare.First == Second && toCompare.Second == First;
            }
        }

        private IEnumerable<DecisionPair> GetAllPossiblePairs()
        {
            var alternatives = _context.Alternatives.ToList();
            var allPairs = (from a1 in alternatives
                from a2 in alternatives
                where a1.Id != a2.Id
                select new { First = a1.Id, Second = a2.Id }).ToList();

            var distinct = allPairs
                .Select(s => new List<int> { s.First, s.Second }.OrderBy(s1 => s1))
                .Select(pair => pair.First() + "-" + pair.Last())
                .Distinct()
                .Select(str => str.Split("-").Select(int.Parse).ToList())
                .Select(pairs => new DecisionPair { First = pairs.First(), Second = pairs.Last() });

            return distinct;
        }

        // POST: Decisions/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int id, [Bind("First,Second,Chosen")] DecisionPair pair)
        {
            var currentPairs = HttpContext.Session.GetObject<IEnumerable<DecisionPair>>("todo" + id);

            currentPairs.First(decisionPair => decisionPair.Equals(pair)).Chosen = pair.Chosen;

            if (currentPairs.All(decisionPair => decisionPair.Chosen.HasValue))
            {
                var chosen = currentPairs.Select(decisionPair => decisionPair.Chosen.Value);

                var wins = _context.Alternatives.ToDictionary(
                    alternative => alternative.Id,
                    alternative => chosen.Count(chosenId => chosenId == alternative.Id))
                    .OrderByDescending(valuePair => valuePair.Value)
                    .ToList();

                var rank = 1;
                var person = _context.Persons.Find(id);

                _context.Decisions.RemoveRange(_context.Decisions.Where(decision => decision.PersonId == person.Id));

                while (wins.Any())
                {
                    var max = wins.Max(valuePair => valuePair.Value);

                    var winners = wins.Where(valuePair => valuePair.Value == max).ToList();

                    foreach (var winner in winners)
                    {
                        _context.Decisions.Add(new Decision
                        {
                            AlternativeId = winner.Key,
                            PersonId = person.Id,
                            Rank = rank,
                            Weight = person.CompetencyLevel
                        });
                    }

                    wins.RemoveRange(0, winners.Count);

                    rank++;
                }

                _context.SaveChanges();

                HttpContext.Session.Remove("todo" + id);

                return RedirectToAction(nameof(Index));
            }

            HttpContext.Session.SetObject("todo" + id, currentPairs);

            return RedirectToAction(nameof(Create), new { id = id } );
        }

        // GET: Decisions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var decision = await _context.Decisions.FindAsync(id);
            if (decision == null)
            {
                return NotFound();
            }
            ViewData["AlternativeId"] = new SelectList(_context.Alternatives, "Id", "Name", decision.AlternativeId);
            ViewData["PersonId"] = new SelectList(_context.Persons, "Id", "Name", decision.PersonId);
            return View(decision);
        }

        // POST: Decisions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Rank,Weight,PersonId,AlternativeId,Id")] Decision decision)
        {
            if (id != decision.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(decision);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DecisionExists(decision.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AlternativeId"] = new SelectList(_context.Alternatives, "Id", "Name", decision.AlternativeId);
            ViewData["PersonId"] = new SelectList(_context.Persons, "Id", "Name", decision.PersonId);
            return View(decision);
        }

        // GET: Decisions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var decision = await _context.Decisions
                .Include(d => d.Alternative)
                .Include(d => d.Person)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (decision == null)
            {
                return NotFound();
            }

            return View(decision);
        }

        // POST: Decisions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var decision = await _context.Decisions.FindAsync(id);
            _context.Decisions.Remove(decision);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DecisionExists(int id)
        {
            return _context.Decisions.Any(e => e.Id == id);
        }
    }
}
