﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Mvc;
using DecisionPortal.Web.Models;
using DecisionPortal.Web.Models.ViewModels;

namespace DecisionPortal.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly DecisionPortalDbContext _context;

        public HomeController(DecisionPortalDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var alternativeMarks = new List<AlternativeMarksViewModel>();

            var alternatives = _context.Alternatives.ToList();

            foreach (var alternative in alternatives)
            {
                var alternativeMark = new AlternativeMarksViewModel
                {
                    Alternative = alternative,
                    Marks = new Dictionary<Criterion, IList<Mark>>()
                };

                var criteria = _context.Criteria.ToList();

                foreach (var criterion in criteria)
                {
                    var marks = _context.AlternativeMarks
                        .Where(altMark => altMark.AlternativeId == alternative.Id)
                        .Select(altMark => altMark.Mark)
                        .Where(mark => mark.CriterionId == criterion.Id)
                        .ToList();

                    alternativeMark.Marks.Add(criterion, marks);
                }

                alternativeMarks.Add(alternativeMark);
            }


            return View(alternativeMarks);
        }

        public IActionResult AddVector(int id)
        {
            var addAlternativeMarks = new AddAlternativeMarkViewModel
            {
                Alternative = _context.Alternatives.Find(id),
                SelectedMarkIds = _context.AlternativeMarks.Where(mark => mark.AlternativeId == id).Select(mark => mark.MarkId).ToList(),
                CriterionMarks = _context.Criteria.Select(criterion => new CriterionMarkViewModel
                {
                    Criterion = criterion,
                    AvailableMarks = _context.Marks.Where(mark => mark.CriterionId == criterion.Id)
                })
            };

            return View(addAlternativeMarks);
        }

        [HttpPost]
        public IActionResult AddVector(AddAlternativeMarkViewModel model)
        {
            var existingVector = _context.AlternativeMarks
                .Where(alternativeMark => alternativeMark.AlternativeId == model.Alternative.Id);

            _context.AlternativeMarks.RemoveRange(existingVector);

            _context.SaveChanges();

            _context.AlternativeMarks.AddRange(model.SelectedMarkIds.Select(markId => new AlternativeMark
            {
                AlternativeId = model.Alternative.Id,
                MarkId = markId
            }));

            _context.SaveChanges();

            return RedirectToAction(nameof(Index));
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
