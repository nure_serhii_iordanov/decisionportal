﻿using DecisionPortal.Web.Models;
using Microsoft.EntityFrameworkCore;

namespace DecisionPortal.Web
{
    public class DecisionPortalDbContext : DbContext
    {
        public DecisionPortalDbContext(DbContextOptions<DecisionPortalDbContext> options)
            : base(options)
        {
        }

        public DecisionPortalDbContext()
        {
        }

        public DbSet<Person> Persons { get; set; }

        public DbSet<Alternative> Alternatives { get; set; }

        public DbSet<AlternativeMark> AlternativeMarks { get; set; }

        public DbSet<Criterion> Criteria { get; set; }

        public DbSet<Decision> Decisions { get; set; }

        public DbSet<Mark> Marks { get; set; }
    }
}
